{
  "name": "Docker",
  "description": "Check, build, test, audit and publish a [Docker](https://www.docker.com/) image",
  "template_path": "templates/gitlab-ci-docker.yml",
  "kind": "package",
  "variables": [
    {
      "name": "DOCKER_KANIKO_IMAGE",
      "description": "The Docker image used to run kaniko\n\n_for kaniko build only_",
      "default": "gcr.io/kaniko-project/executor:debug"
    },
    {
      "name": "DOCKER_IMAGE",
      "description": "The Docker image used to run the docker client\n\n_for Docker-in-Docker build only_",
      "default": "docker:latest"
    },
    {
      "name": "DOCKER_DIND_IMAGE",
      "description": "The Docker image used to run the Docker daemon\n\n_for Docker-in-Docker build only_",
      "default": "docker:dind"
    },
    {
      "name": "DOCKER_SKOPEO_IMAGE",
      "description": "The docker image used to publish docker image with Skopeo",
      "default": "quay.io/skopeo/stable:latest"
    },
    {
      "name": "DOCKER_FILE",
      "description": "The path to your `Dockerfile`",
      "default": "$CI_PROJECT_DIR/Dockerfile"
    },
    {
      "name": "DOCKER_CONTEXT_PATH",
      "description": "The Docker [context path](https://docs.docker.com/engine/reference/commandline/build/#build-with-path) (working directory) - _only set if you want a context path different from the Dockerfile location_",
      "advanced": true
    },
    {
      "name": "DOCKER_SNAPSHOT_IMAGE",
      "description": "Docker snapshot image",
      "default": "$CI_REGISTRY_IMAGE/snapshot:$CI_COMMIT_REF_SLUG"
    },
    {
      "name": "DOCKER_RELEASE_IMAGE",
      "description": "Docker release image",
      "default": "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    },
    {
      "name": "DOCKER_BUILD_ARGS",
      "description": "Additional docker build/kaniko arguments"
    },
    {
      "name": "DOCKER_PUBLISH_ARGS",
      "description": "Additional [`skopeo copy` arguments](https://github.com/containers/skopeo/blob/master/docs/skopeo-copy.1.md#options)"
    },
    {
      "name": "PUBLISH_ON_PROD",
      "description": "Determines whether this job is enabled on `master` branch",
      "default": "true",
      "type": "boolean"
    },
    {
      "name": "DOCKER_REGISTRY_MIRROR",
      "description": "URL of a Docker registry mirror to use instead of default `https://index.docker.io`"
    }
  ],
  "features": [
    {
      "id": "lint",
      "name": "dockerfile-lint",
      "description": "This job performs a [Lint](https://github.com/projectatomic/dockerfile_lint) on your `Dockerfile`.",
      "enable_with": "DOCKER_LINT_ENABLED",
      "variables": [
        {
          "name": "DOCKER_LINT_IMAGE",
          "description": "The docker image to lint your Dockerfile",
          "default": "projectatomic/dockerfile-lint:latest"
        },
        {
          "name": "DOCKER_LINT_ARGS",
          "description": "Additional `dockerfile_lint` arguments",
          "advanced": true
        }
      ]
    },
    {
      "id": "hadolint",
      "name": "Hadolint",
      "description": "This job performs a [Lint](https://github.com/hadolint/hadolint) on your `Dockerfile`.",
      "disable_with": "DOCKER_HADOLINT_DISABLED",
      "variables": [
        {
          "name": "DOCKER_HADOLINT_IMAGE",
          "description": "The docker image to lint your Dockerfile with Hadolint",
          "default": "hadolint/hadolint:latest-alpine"
        },
        {
          "name": "DOCKER_HADOLINT_ARGS",
          "description": "Additional `hadolint` arguments",
          "advanced": true
        }
      ]
    },
    {
      "id": "dind",
      "name": "Docker-in-Docker",
      "description": "Use Docker-in-Docker to build the image (instead of Kaniko)\n\n_Warning: unsecured, requires privileged runners_",
      "enable_with": "DOCKER_DIND_BUILD"
    },
    {
      "id": "healthcheck",
      "name": "Health Check",
      "description": "[Health Check](https://docs.docker.com/engine/reference/builder/#healthcheck) image analysis",
      "disable_with": "DOCKER_HEALTHCHECK_DISABLED",
      "variables": [
        {
          "name": "DOCKER_HEALTHCHECK_TIMEOUT",
          "type": "number",
          "description": "When testing an image, how long (in seconds) wait for the HealthCheck status",
          "default": "60",
          "advanced": true
        },
        {
          "name": "DOCKER_HEALTHCHECK_OPTIONS",
          "description": "Docker options for health check such as port mapping, environment...",
          "advanced": true
        },
        {
          "name": "DOCKER_HEALTHCHECK_CONTAINER_ARGS",
          "description": "Arguments sent to the running container for health check",
          "advanced": true
        }
      ]
    },
    {
      "id": "trivy",
      "name": "Trivy",
      "description": "[Trivy](https://github.com/aquasecurity/trivy) vulnerability analysis",
      "disable_with": "DOCKER_TRIVY_DISABLED",
      "variables": [
        {
          "name": "DOCKER_TRIVY_IMAGE",
          "description": "The docker image used to scan images with Trivy",
          "default": "aquasec/trivy:latest",
          "advanced": true
        },
        {
          "name": "DOCKER_TRIVY_ADDR",
          "type": "url",
          "description": "The Trivy server address",
          "mandatory": true
        },
        {
          "name": "DOCKER_TRIVY_SECURITY_LEVEL_THRESHOLD",
          "type": "enum",
          "values": ["UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL", "LOW,MEDIUM,HIGH,CRITICAL", "MEDIUM,HIGH,CRITICAL", "HIGH,CRITICAL", "CRITICAL"],
          "description": "Severities of vulnerabilities to be displayed (comma separated values: `UNKNOWN`, `LOW`, `MEDIUM`, `HIGH`, `CRITICAL`)",
          "default": "UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL"
        }
      ]
    }
  ]
}
